package jp.gr.java_conf.ofnhwx.olib.base;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;

/**
 * {@link SimpleCursorAdapter}のもと.
 * <table>
 *  <tr><td>・コンストラクタで{@link #setViewBinder(ViewBinder)}の実行</td></tr>
 *  <tr><td>・{@link #swapCursor(Cursor)}で{@link #onSetColumns(Cursor)}を呼出し</td></tr>
 * </table>
 * @author yuta
 */
public abstract class BaseCursorAdapter extends SimpleCursorAdapter implements ViewBinder {

    /**
     * {@link #swapCursor(Cursor)}から呼出し.項目の列取得などが可能.
     * @param c 新しいカーソル.
     */
    protected abstract void onSetColumns(Cursor c);

    public BaseCursorAdapter(Context context, int layout, String[] from, int[] to, int flags) {
        super(context, layout, null, from, to, flags);
        setViewBinder(this);
    }

    @Override
    public Cursor swapCursor(Cursor c) {
        if (c == null) {
            return super.swapCursor(c);
        } else {
            Cursor result = super.swapCursor(c);
            onSetColumns(c);
            return result;
        }
    }

}
