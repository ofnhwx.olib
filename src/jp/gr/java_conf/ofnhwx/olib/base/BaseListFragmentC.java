package jp.gr.java_conf.ofnhwx.olib.base;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

/**
 * {@link ListFragment}のもと.
 * @author yuta
 */
public abstract class BaseListFragmentC extends ListFragment implements LoaderCallbacks<Cursor> {

    protected CursorAdapter mAdapter;

    protected abstract CursorAdapter onCreateAdapter();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = onCreateAdapter();
        setListAdapter(mAdapter);
        setListShown(false);
        getListView().setFastScrollEnabled(true);
    }

    /**
     * {@link #setEmptyText(CharSequence)}.
     * @param text {@link #getText(int)}
     */
    public void setEmptyText(int text) {
        setEmptyText(getText(text));
    }

    @Override
    public void setEmptyText(CharSequence text) {
        try {
            super.setEmptyText(text);
        } catch (IllegalStateException e) {
            TextView ev = (TextView)getListView().getEmptyView();
            if (ev == null) {
                TextView tv = new TextView(getActivity());
                tv.setText(text);
                tv.setId(android.R.id.empty);
                ViewParent v = getListView().getParent();
                if (v instanceof ViewGroup) {
                    ((ViewGroup)v).addView(tv);
                }
                getListView().setEmptyView(tv);
            } else {
                ev.setText(text);
            }
        }
    }

    public void setHasContextMenu(boolean hasContext) {
        if (hasContext) {
            registerForContextMenu(getListView());
        } else {
            unregisterForContextMenu(getListView());
        }
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (getView() == null) {
            return;
        }
        mAdapter.swapCursor(data);
        if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

}
