package jp.gr.java_conf.ofnhwx.olib.base;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * {@link SQLiteOpenHelper}のもと, 継承して使ってね.
 * @author yuta
 */
public abstract class BaseSQLiteOpenHelper extends SQLiteOpenHelper {

    public static enum SQLiteType {
        ID      { @Override public String toString() { return "INTEGER PRIMARY KEY AUTOINCREMENT"; } },
        NULL    { @Override public String toString() { return "NULL"   ; } },
        INTEGER { @Override public String toString() { return "INTEGER"; } },
        REAL    { @Override public String toString() { return "REAL"   ; } },
        TEXT    { @Override public String toString() { return "TEXT"   ; } },
        BLOB    { @Override public String toString() { return "BLOB"   ; } },
    }

    public BaseSQLiteOpenHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    protected void createTable(SQLiteDatabase db, String tableName, String[] names, SQLiteType[] types) {
        if (names.length != types.length) {
            throw new IllegalArgumentException("names count must be same as types count.");
        }
        LinkedHashMap<String, SQLiteType> columns = new LinkedHashMap<String, BaseSQLiteOpenHelper.SQLiteType>();
        for (int i = 0; i < names.length; ++i) {
            columns.put(names[i], types[i]);
        }
        createTable(db, tableName, columns);
    }

    protected void createTable(SQLiteDatabase db, String tableName, LinkedHashMap<String, SQLiteType> columns) {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE ").append(tableName).append(" (");
        for (Entry<String, SQLiteType> column : columns.entrySet()) {
            sql.append(column.getKey());
            sql.append(" ");
            sql.append(column.getValue().toString());
            sql.append(",");
        }
        sql.replace(sql.length() - 1, sql.length(), ");");
        db.execSQL(sql.toString());
    }

    protected void deleteTable(SQLiteDatabase db, String tableName) {
        db.execSQL("DROP TABLE IF EXISTS " + tableName);
    }

}
