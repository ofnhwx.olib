package jp.gr.java_conf.ofnhwx.olib.base;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import android.app.Notification;
import android.app.Service;

/**
 * サービスの互換性対策.
 * @author yuta
 */
public abstract class BaseService extends Service {

    private static final Class<?>[] setForegroundSignature   = new Class[] { boolean.class };
    private static final Class<?>[] startForegroundSignature = new Class[] { int.class, Notification.class };
    private static final Class<?>[] stopForegroundSignature  = new Class[] { boolean.class };

    private Method setForeground;
    private Method startForeground;
    private Method stopForeground;

    private Object[] setForegroundArgs   = new Object[1];
    private Object[] startForegroundArgs = new Object[2];
    private Object[] stopForegroundArgs  = new Object[1];

    private static Map<String, BaseService> services = new HashMap<String, BaseService>();
    private boolean isForeground;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            startForeground = getClass().getMethod("startForeground", startForegroundSignature);
            stopForeground  = getClass().getMethod("stopForeground" , stopForegroundSignature );
        } catch (Exception e) {
            startForeground = null;
            stopForeground  = null;
        }
        try {
            setForeground = getClass().getMethod("setForeground", setForegroundSignature);
        } catch (Exception e) {
            setForeground = null;
        }
        services.put(getClass().getName(), this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        services.put(getClass().getName(), this);
    }

    /**
     * {@link #isForeground(Class)}を参照.
     */
    public boolean isForeground() {
        return isForeground(getClass());
    }

    /**
     * {@link #startForegroundCompat(Class, int, Notification)}を参照.
     */
    public void startForegroundCompat(int id, Notification notification) {
        startForegroundCompat(getClass(), id, notification);
    }

    /**
     * {@link #stopForegroundCompat(Class)}を参照.
     */
    public void stopForegroundCompat() {
        stopForegroundCompat(getClass());
    }

    /**
     * サービスのフォアグラウンド設定を取得.
     * @param clazz {@link OService#getClass()}
     * @return true:サビースはフォアグラウンドに設定されている
     */
    public static final boolean isForeground(Class<? extends BaseService> clazz) {
        BaseService service = services.get(clazz.getName());
        if (service == null) {
            return false;
        } else {
            return service.isForeground;
        }
    }

    /**
     * サービスをフォアグラウンドにする.
     * @param clazz {@link OService#getClass()}
     * @param id 通知のID
     * @param notification 通知オブジェクト
     */
    public static final void startForegroundCompat(Class<? extends BaseService> clazz, int id, Notification notification) {
        BaseService service = services.get(clazz.getName());
        if (service == null || service.isForeground) {
            return;
        }
        try {
            if (service.startForeground != null) {
                service.startForegroundArgs[0] = Integer.valueOf(id);
                service.startForegroundArgs[1] = notification;
                service.startForeground.invoke(service, service.startForegroundArgs);
                return;
            }
            service.setForegroundArgs[0] = Boolean.TRUE;
            service.setForeground.invoke(service, service.setForegroundArgs);
        } catch (Exception e) {
            // do nothing
        } finally {
            service.isForeground = true;
        }
    }

    /**
     * サービスのフォアグラウンド設定を解除する.
     * @param clazz {@link OService#getClass()}
     */
    public static final void stopForegroundCompat(Class<? extends BaseService> clazz) {
        BaseService service = services.get(clazz.getName());
        if (service == null || !service.isForeground) {
            return;
        }
        try {
            if (service.stopForeground != null) {
                service.stopForegroundArgs[0] = Boolean.TRUE;
                service.stopForeground.invoke(service, service.stopForegroundArgs);
                return;
            }
            service.setForegroundArgs[0] = Boolean.FALSE;
            service.setForeground.invoke(service, service.setForegroundArgs);
        } catch (Exception e) {
            // do nothing
        } finally {
            service.isForeground = false;
        }
    }

}
