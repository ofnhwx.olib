package jp.gr.java_conf.ofnhwx.olib.base;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

/**
 * {@link ContentProvider}のもと.
 * @author yuta
 */
public abstract class BaseContentProvider extends ContentProvider {

	/**
	 * 指定されたパラメータが設定されているか確認し、
	 * 設定されていなければ{@link IllegalArgumentException}を投げる.
	 * @param values
	 * @param key
	 */
    public void check(ContentValues values, String key) {
        if (!values.containsKey(key)) {
            final String format = "Parameter `%s' is must be specified.";
            throw new IllegalArgumentException(String.format(format, key));
        }
    }

    public void checkAndSet(ContentValues values, String key, Boolean def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, Byte def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, byte[] def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, Double def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, Float def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, Integer def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, Long def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, Short def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    public void checkAndSet(ContentValues values, String key, String def) {
        if (!values.containsKey(key)) {
            values.put(key, def);
        }
    }

    /**
     * {@link Uri}から<code>where句</code>を作成する.
     * @param uri {@link ContentUris#withAppendedId(Uri, long)}
     * @return _id=(${@link ContentUris#parseId(Uri)})
     */
    public String whereWithId(Uri uri) {
        return whereWithId(uri, null);
    }

    /**
     * {@link Uri}から<code>where句</code>を作成する.
     * @param uri {@link ContentUris#withAppendedId(Uri, long)}
     * @param where 追加の条件文
     * @return _id=(${@link ContentUris#parseId(Uri)}) and ($where)
     */
    public String whereWithId(Uri uri, String where) {
        StringBuilder sb = new StringBuilder();
        sb.append(BaseColumns._ID).append("=").append(ContentUris.parseId(uri));
        if (!TextUtils.isEmpty(where)) {
            sb.append(" and (").append(where).append(")");
        }
        return sb.toString();
    }

}
