package jp.gr.java_conf.ofnhwx.olib.base;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

/**
 * {@link PreferenceActivity}のもと.
 * @author yuta
 */
public abstract class BasePreferenceActivity extends PreferenceActivity {

    OnSharedPreferenceChangeListener listener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this instanceof OnSharedPreferenceChangeListener) {
            listener = (OnSharedPreferenceChangeListener)this;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (listener != null) {
            SharedPreferences preferences = getPreferenceScreen().getSharedPreferences();
            preferences.registerOnSharedPreferenceChangeListener(listener);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (listener != null) {
            SharedPreferences preferences = getPreferenceScreen().getSharedPreferences();
            preferences.unregisterOnSharedPreferenceChangeListener(listener);
        }
    }

    public void setChecked(int key, boolean checked) {
        CheckBoxPreference preference = findPreference(key);
        preference.setChecked(checked);
    }

    @SuppressWarnings("unchecked")
    public <T extends Preference> T findPreference(String key) {
        return (T)super.findPreference(key);
    }

    public <T extends Preference> T findPreference(int key) {
        return findPreference(getString(key));
    }

}
