package jp.gr.java_conf.ofnhwx.olib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Thread.UncaughtExceptionHandler;

import jp.gr.java_conf.ofnhwx.olib.utils.OBitmap;
import jp.gr.java_conf.ofnhwx.olib.utils.OUtil;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

/**
 * バグ報告用のクラス(頂いてきた).
 * @author yuta
 */
public final class ErrorReporter implements UncaughtExceptionHandler {

    private static final String TAG = ErrorReporter.class.getSimpleName();

    private static final UncaughtExceptionHandler DEFAULT_HANDLER = Thread.getDefaultUncaughtExceptionHandler();
    private static final String ERROR_FILE  = "BUG";
    private static final String REPORT_FILE = "REPORT";
    private static Context     sContext     = null;
    private static PackageInfo sPackageInfo = null;

    /**
     * バグ報告の準備、各Activityのなるべく早い段階で呼び出すこと.
     * @param context 基本的には{@code this}を指定
     */
    public static void initialize(Context context) {

        // 2回目以降は何もしない
        if (sContext != null) {
            return;
        }

        // 標準の例外処理をすり替える
        sContext = context.getApplicationContext();
        try {
            PackageManager pm = sContext.getPackageManager();
            sPackageInfo = pm.getPackageInfo(sContext.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        Thread.setDefaultUncaughtExceptionHandler(new ErrorReporter());

    }

    /**
     * エラー報告の確認を行うダイアログを表示する.
     * @param activity ダイアログを表示するアクティビティ
     * @param dialogId 表示するダイアログの{@code id}
     */
    public static final void show(Activity activity, int dialogId) {
        File file = activity.getFileStreamPath(ERROR_FILE);
        if (!file.exists()) {
            return;
        }
        activity.showDialog(dialogId);
    }

    /**
     * エラー報告の確認を行うダイアログを取得する.
     * @param activity
     * @param icon
     * @return
     */
    public static final AlertDialog getDialog(final Activity activity, int icon) {

        // エラーファイル
        final File file = activity.getFileStreamPath(ERROR_FILE);

        // 表示するアイコン
        float density = activity.getResources().getDisplayMetrics().density;
        Bitmap bitmap = OBitmap.decodeResource(activity, icon, (int)(32 * density));

        // ダイアログの初期設定
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setIcon(new BitmapDrawable(bitmap));
        dialog.setTitle(R.string.bug_report_title);
        dialog.setMessage(R.string.bug_report_message);

        // エラー内容を送信する
        dialog.setPositiveButton(R.string.bug_report_positive, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                sendBugreport(activity, file);
            }
        });

        // エラー内容を送信しない
        dialog.setNegativeButton(R.string.bug_report_negative, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (!file.delete()) {
                    Log.e(TAG, "Couldn't delete error file.");
                }
            }
        });

        // キャンセル
        dialog.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                if (!file.delete()) {
                    Log.e(TAG, "Couldn't delete error file.");
                }
            }
        });

        // ダイアログを返す
        return dialog.create();

    }

    /**
     * エラー報告の送信.
     * @param activity
     * @param errorFile
     * @throws IOException
     */
    private static final void sendBugreport(Context context, File errorFile) {

        // バグ報告ファイルをリネーム
        File file = context.getFileStreamPath(REPORT_FILE);
        if (!errorFile.exists() || !errorFile.renameTo(file)) {
            return;
        }

        //
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            // 何もしない
            return;
        }

        //
        String subject;
        StringBuilder text = new StringBuilder();
        try {
            subject = reader.readLine();
            text.append(subject).append("\n");
            String line;
            while ((line = reader.readLine()) != null) {
                text.append(line).append("\n");
            }
        } catch (IOException e) {
            return;
        } finally {
            try { reader.close(); } catch (Exception e) {}
            if (!file.delete()) {
                Log.e(TAG, "Can't delete report file.");
            }
        }

        // メールの送信
        String[] mail = new String[] { context.getString(R.string.mail) };
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, mail);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, text.toString());
        try {
            OUtil.sendChoiceIntent(context, intent, R.string.choose_mailer);
        } catch (ActivityNotFoundException e) {
            String mailTo = context.getString(R.string.mail_to);
            intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(mailTo));
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, text.toString());
            try {
                OUtil.sendChoiceIntent(context, intent, R.string.choose_mailer);
            } catch (ActivityNotFoundException e2) {
                Toast.makeText(context, "Activity not found.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    public void uncaughtException(Thread thread, Throwable error) {

        // スタックトレースの出力
        error.printStackTrace();

        // ファイルの準備
        PrintWriter writer;
        try {
            FileOutputStream fos = sContext.openFileOutput(ERROR_FILE, Context.MODE_WORLD_READABLE);
            writer = new PrintWriter(fos);
        } catch (FileNotFoundException e) {
            // ここには到達しない、しても何もしない
            return;
        }

        // パッケージ情報の記録
        if (sPackageInfo != null) {
            writer.printf("[BUG][%s] versionName:%s, versionCode:%d\n",
                            sPackageInfo.packageName,
                            sPackageInfo.versionName,
                            sPackageInfo.versionCode);
        } else {
            writer.printf("[BUG][Unkown Package]\n");
        }

        // メモリ情報の記録(Total, Free, Used)
        long total = Runtime.getRuntime().totalMemory() / 1024;
        long free  = Runtime.getRuntime().freeMemory()  / 1024;
        writer.printf("Runtime Memory: total: %dKB, free: %dKB, used: %dKB\n",
                        total, free, total - free);

        // メモリ情報の記録(Avail, LowMemory)
        ActivityManager am = (ActivityManager)sContext.getSystemService(Context.ACTIVITY_SERVICE);
        MemoryInfo info = new MemoryInfo();
        am.getMemoryInfo(info);
        writer.printf("availMem: %dKB, lowMemory: %b\n",
                        info.availMem / 1024, info.lowMemory);

        // デバイス情報の記録
        writer.printf("DEVICE: %s\n", Build.DEVICE);
        writer.printf("MODEL : %s\n", Build.MODEL);
        writer.printf("VERSION.SDK: %s\n", Build.VERSION.SDK);
        writer.printf("\n");

        // スタックトレースを記録
        error.printStackTrace(writer);

        // ファイルを保存して閉じる
        writer.close();

        // ここで終了
        DEFAULT_HANDLER.uncaughtException(thread, error);

    }

}
