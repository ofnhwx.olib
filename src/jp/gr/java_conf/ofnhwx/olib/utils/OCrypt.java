package jp.gr.java_conf.ofnhwx.olib.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * 暗号用ライブラリ.
 * @author yuta
 */
public final class OCrypt {

    private static final String ALGORITHM_AES = "AES";
    private static final String ALGORITHM_MD5 = "MD5";
    private static final String ALGORITHM_SHA = "SHA1PRNG";

    private static final String AES_ECB_PKCS5 = "AES/ECB/PKCS5Padding";
    private static final String AES_CBC_PKCS5 = "AES/CBC/PKCS5Padding";

    private OCrypt() {}

    /**
     * CBC形式で使用するデータ.
     */
    public static class EncryptedData {
        public byte[] iv;
        public byte[] data;
        public EncryptedData(byte[] iv, byte[] data) {
            this.iv   = iv;
            this.data = data;
        }
    }

    /**
     * 暗号化失敗.
     */
    @SuppressWarnings("serial")
    public static class BadEncryptException extends Exception {
        public BadEncryptException(Throwable e) {
            super(e);
        }
    }

    /**
     * 復号失敗.
     */
    @SuppressWarnings("serial")
    public static class BadDecryptException extends Exception {
        public BadDecryptException(Throwable e) {
            super(e);
        }
    }

    /**
     * ダイジェスト生成失敗.
     */
    @SuppressWarnings("serial")
    public static class BadDigestException extends Exception {
        public BadDigestException(Throwable e) {
            super(e);
        }
    }

    /**
     * データの暗号化(ECB).
     * @param key  暗号化に使用するキー
     * @param data 暗号化するデータ
     * @return 暗号化されたデータ
     */
    public static final byte[] encryptECB(byte[] key, byte[] data) {
        try {
            return encryptECBwithThrow(key, data);
        } catch (BadEncryptException e) {
            return null;
        }
    }

    /**
     * データの暗号化(ECB).
     * @param key  暗号化に使用するキー
     * @param data 暗号化するデータ
     * @return 暗号化されたデータ
     * @throws BadEncryptException 暗号化に失敗.
     */
    public static final byte[] encryptECBwithThrow(byte[] key, byte[] data) throws BadEncryptException {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM_AES);
            Cipher cipher = Cipher.getInstance(AES_ECB_PKCS5);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            return cipher.doFinal(data);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new BadEncryptException(e);
        }
    }

    /**
     * データの復号(ECB).
     * @param key  復号に使用するキー
     * @param data 復号するデータ
     * @return 復号されたデータ
     */
    public static final byte[] decryptECB(byte[] key, byte[] data) {
        try {
            return decryptECBwithThrow(key, data);
        } catch (BadDecryptException e) {
            return null;
        }
    }

    /**
     * データの復号(ECB).
     * @param key  復号に使用するキー
     * @param data 復号するデータ
     * @return 復号されたデータ
     * @throws BadDecryptException 復号に失敗
     */
    public static final byte[] decryptECBwithThrow(byte[] key, byte[] data) throws BadDecryptException {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM_AES);
            Cipher cipher = Cipher.getInstance(AES_ECB_PKCS5);
            cipher.init(Cipher.DECRYPT_MODE, keySpec);
            return cipher.doFinal(data);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new BadDecryptException(e);
        }
    }

    /**
     * データの暗号化(CBC).
     * @param key  暗号化に使用するキー
     * @param data 暗号化するデータ
     * @return 暗号化されたデータ
     */
    public static final EncryptedData encryptCBC(byte[] key, byte[] data) {
        try {
            return encryptCBCwithThrow(key, data);
        } catch (BadEncryptException e) {
            return null;
        }
    }

    /**
     * データの暗号化(CBC).
     * @param key  暗号化に使用するキー
     * @param data 暗号化するデータ
     * @return 暗号化されたデータ
     * @throws BadEncryptException 暗号化に失敗
     */
    public static final EncryptedData encryptCBCwithThrow(byte[] key, byte[] data) throws BadEncryptException {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM_AES);
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5);
            SecureRandom random = SecureRandom.getInstance(ALGORITHM_SHA);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, random);
            return new EncryptedData(cipher.getIV(), cipher.doFinal(data));
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new BadEncryptException(e);
        }
    }

    /**
     * データの復号(CBC).
     * @param key  復号に使用するキー
     * @param data 復号するデータ
     * @return 復号されたデータ
     */
    public static final byte[] decryptCBC(byte[] key, EncryptedData data) {
        try {
            return decryptCBCwithThrow(key, data);
        } catch (BadDecryptException e) {
            return null;
        }
    }

    /**
     * データの復号(CBC).
     * @param key  復号に使用するキー
     * @param data 復号するデータ
     * @return 復号されたデータ
     * @throws BadDecryptException 復号に失敗
     */
    public static final byte[] decryptCBCwithThrow(byte[] key, EncryptedData data) throws BadDecryptException {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, ALGORITHM_AES);
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(data.iv));
            return cipher.doFinal(data.data);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new BadDecryptException(e);
        }
    }

    /**
     * MD5ダイジェストの取得.
     * @param data MD5ダイジェストを取得するデータ
     * @return MD5ダイジェスト
     */
    public static final byte[] digestMD5(byte[] data) {
        try {
            return digestMD5withThrow(data);
        } catch (BadDigestException e) {
            return null;
        }
    }

    /**
     * MD5ダイジェストの取得.
     * @param data MD5ダイジェストを取得するデータ
     * @return MD5ダイジェスト
     * @throws BadDigestException
     */
    public static final byte[] digestMD5withThrow(byte[] data) throws BadDigestException {
        try {
            MessageDigest digest = MessageDigest.getInstance(ALGORITHM_MD5);
            return digest.digest(data);
        } catch (RuntimeException e) {
            throw e;
        } catch (NoSuchAlgorithmException e) {
            throw new BadDigestException(e);
        }
    }

}
