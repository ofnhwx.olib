package jp.gr.java_conf.ofnhwx.olib.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;

/**
 * デフォルト設定の取得・設定を行うクラス.
 * @author yuta
 */
public final class OPreference {

    // 唯一のインスタンス
    private static OPreference sInstance = null;

    // 各種定数値を定義
    private static enum Type {
        BOOLEAN,
        FLOAT,
        INT,
        LONG,
        STRING;
    }
    private final Context context;
    private final SharedPreferences preferences;

    /**
     * インスタンスの取得.
     * @param context
     * @return {@link OPreference}のインスタンス
     */
    public static OPreference getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new OPreference(context.getApplicationContext());
        }
        return sInstance;
    }

    private OPreference(Context context) {
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (isGalaxyS()) {
            readFromFile();
        }
    }

    public void flush() {
        if (isGalaxyS()) {
            writeToFile();
        }
    }

    /**
     * デフォルト設定を更新する.
     * @param key 更新するデフォルト設定の名前
     * @param value 新しい値
     * @param type 値の型
     * @return 更新に成功したかどうかを返す
     * @throws IllegalArgumentException たぶん発生することはない
     */
    private boolean write(String key, Object value, Type type) {
        SharedPreferences.Editor editor = preferences.edit();
        switch (type) {
        case BOOLEAN:
            editor.putBoolean(key, (Boolean)value);
            break;
        case FLOAT:
            editor.putFloat(key, (Float)value);
            break;
        case INT:
            editor.putInt(key, (Integer)value);
            break;
        case LONG:
            editor.putLong(key, (Long)value);
            break;
        case STRING:
            editor.putString(key, (String)value);
            break;
        default:
            throw new IllegalArgumentException("Unrecognized type: " + value.getClass().getSimpleName());
        }
        return editor.commit();
    }

    /**
     * デフォルト設定を更新する(boolean値)
     * @param key 更新するデフォルト設定の名前
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(String key, boolean value) {
        return write(key, value, Type.BOOLEAN);
    }

    /**
     * デフォルト設定を更新する(float値)
     * @param key 更新するデフォルト設定の名前
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(String key, float value) {
        return write(key, value, Type.FLOAT);
    }

    /**
     * デフォルト設定を更新する(int値)
     * @param key 更新するデフォルト設定の名前
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(String key, int value) {
        return write(key, value, Type.INT);
    }

    /**
     * デフォルト設定を更新する(long値)
     * @param key 更新するデフォルト設定の名前
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(String key, long value) {
        return write(key, value, Type.LONG);
    }

    /**
     * デフォルト設定を更新する(String値)
     * @param key 更新するデフォルト設定の名前
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(String key, String value) {
        return write(key, value, Type.STRING);
    }

    /**
     * デフォルト設定を更新する(boolean値)
     * @param key 更新するデフォルト設定のリソースID
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(int resId, boolean value) {
        return write(context.getString(resId), value, Type.BOOLEAN);
    }

    /**
     * デフォルト設定を更新する(float値)
     * @param key 更新するデフォルト設定のリソースID
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(int resId, float value) {
        return write(context.getString(resId), value, Type.FLOAT);
    }

    /**
     * デフォルト設定を更新する(int値)
     * @param key 更新するデフォルト設定のリソースID
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(int resId, int value) {
        return write(context.getString(resId), value, Type.INT);
    }

    /**
     * デフォルト設定を更新する(long値)
     * @param key 更新するデフォルト設定のリソースID
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(int resId, long value) {
        return write(context.getString(resId), value, Type.LONG);
    }

    /**
     * デフォルト設定を更新する(String値)
     * @param key 更新するデフォルト設定のリソースID
     * @param value 新しい値
     * @return 更新に成功したかどうかを返す
     */
    public boolean write(int resId, String value) {
        return write(context.getString(resId), value, Type.STRING);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定の名前
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定の名前
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定の名前
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定の名前
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定の名前
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定のリソースID
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public boolean getBoolean(int resId, boolean defValue) {
        return preferences.getBoolean(context.getString(resId), defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定のリソースID
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public float getFloat(int resId, float defValue) {
        return preferences.getFloat(context.getString(resId), defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定のリソースID
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public int getInt(int resId, int defValue) {
        return preferences.getInt(context.getString(resId), defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定のリソースID
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public long getLong(int resId, long defValue) {
        return preferences.getLong(context.getString(resId), defValue);
    }

    /**
     * デフォルト設定を取得する
     * @param key 取得するデフォルト設定のリソースID
     * @param defValue デフォルトが設定されていない場合に返す値
     * @return 指定したデフォルト設定の値
     */
    public String getString(int resId, String defValue) {
        return preferences.getString(context.getString(resId), defValue);
    }

    /**
     * 名前からDrawableリソースのIDを取得する
     * @param name 取得するリソースの名前
     * @return リソースID(見つからない場合'0')
     */
    public int getDrawableId(String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }

    /**
     * 名前から文字列リソースのIDを取得する
     * @param name 取得するリソースの名前
     * @return リソースID(見つからない場合'0')
     */
    public int getStringId(String name) {
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }

    // 以下、`Galaxy S 2.2.1'への対策

    private static final String FILENAME = "preferences.dat";

    private boolean isGalaxyS() {
        return Build.MODEL.equals("SC-02B");
    }

    private void readFromFile() {
        FileInputStream   fis = null;
        ObjectInputStream ois = null;
        try {
            fis = context.openFileInput(FILENAME);
            ois = new ObjectInputStream(fis);
            @SuppressWarnings("unchecked")
            Map<String, ?> map = (Map<String, ?>)ois.readObject();
            for (Entry<String, ?> entry : map.entrySet()) {
                Object value = entry.getValue();
                if (value instanceof Boolean) {
                    write(entry.getKey(), (Boolean)value);
                } else  if (value instanceof Float) {
                    write(entry.getKey(), (Float)value);
                } else  if (value instanceof Integer) {
                    write(entry.getKey(), (Integer)value);
                } else  if (value instanceof Long) {
                    write(entry.getKey(), (Long)value);
                } else  if (value instanceof String) {
                    write(entry.getKey(), (String)value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { ois.close(); } catch (Exception e) {}
            try { fis.close(); } catch (Exception e) {}
        }
    }

    private void writeToFile() {
        FileOutputStream   fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = context.openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(preferences.getAll());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { oos.close(); } catch (Exception e) {}
            try { fos.close(); } catch (Exception e) {}
        }
    }

}
