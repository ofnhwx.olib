
package jp.gr.java_conf.ofnhwx.olib.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.text.TextUtils;

/**
 * 電話関連の処理をまとめたユーティリティクラス.
 * @author yuta
 */
public abstract class OPhone {

    private static final String SIP = "sip";
    private static final String TEL = "tel";

    /**
     * 指定された番号に発信(android.permission.CALL_PHONE, android.permission.CALL_PRIVILEGED).
     * @param context
     * @param number
     */
    public static final void call(Context context, String number) {
        call(context, TEL, number);
    }

    /**
     * 指定された番号に発信.
     * @param context
     * @param scheme
     * @param number
     */
    public static final void call(Context context, String scheme, String number) {
        if (TextUtils.isEmpty(number)) {
            return;
        }
        Uri uri = Uri.fromParts(checkScheme(scheme), number, null);
        Intent call = new Intent(Intent.ACTION_CALL, uri);
        call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(call);
    }

    /**
     * 指定されたスキーマが有効かを判定する.
     * @param scheme
     * @return
     */
    public static final boolean checkValidScheme(String scheme) {
        try {
            checkSchemeWithThrow(scheme);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    /**
     * 指定されたスキーマが有効かを判定する.
     * @param scheme
     * @return 指定されてスキーマが有効な場合、そのまま返す
     * @throws NameNotFoundException 指定されたスキーマが無効
     */
    private static final String checkSchemeWithThrow(String scheme)
            throws NameNotFoundException {
        if (SIP.equals(scheme)) {
            return SIP;
        }
        if (TEL.equals(scheme)) {
            return TEL;
        }
        throw new NameNotFoundException();
    }

    /**
     * 指定されたスキーマが有効かを判定し、
     * 無効な場合には`TEL'に置き換えて返す
     * @param scheme
     * @return
     */
    private static final String checkScheme(String scheme) {
        try {
            return checkSchemeWithThrow(scheme);
        } catch (NameNotFoundException e) {
            return TEL;
        }
    }

}
