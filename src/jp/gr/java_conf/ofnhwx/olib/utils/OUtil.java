package jp.gr.java_conf.ofnhwx.olib.utils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * 雑多な機能集.
 * @author yuta
 */
public abstract class OUtil {

    public static enum NumberType {
        /** 通知不可能. */ UNKNOWN ,
        /** 非通知.     */ PRIVATE ,
        /** 公衆電話.   */ PAYPHONE,
        /** 通知.       */ NORMAL  ;
        public static final NumberType get(String number) {
            if (map.containsKey(number)) {
                return map.get(number);
            } else {
                return NumberType.NORMAL;
            }
        }
        // com.android.internal.telephony.CallerInfo
        public static final String UNKNOWN_NUMBER  = "-1";
        public static final String PRIVATE_NUMBER  = "-2";
        public static final String PAYPHONE_NUMBER = "-3";
        public static final String UNKNOWN_NUMBER_NO_HYPHEN  = "1";
        public static final String PRIVATE_NUMBER_NO_HYPHEN  = "2";
        public static final String PAYPHONE_NUMBER_NO_HYPHEN = "3";
        //
        private static final Map<String, NumberType> map = new HashMap<String, OUtil.NumberType>();
        static {
            map.put(UNKNOWN_NUMBER , UNKNOWN);
            map.put(PRIVATE_NUMBER , PRIVATE);
            map.put(PAYPHONE_NUMBER, PAYPHONE);
            map.put(UNKNOWN_NUMBER_NO_HYPHEN , UNKNOWN);
            map.put(PRIVATE_NUMBER_NO_HYPHEN , PRIVATE);
            map.put(PAYPHONE_NUMBER_NO_HYPHEN, PAYPHONE);
        }
        //
        public static final boolean isUnknown(String number) {
            return get(number) == UNKNOWN;
        }
        public static final boolean isPrivate(String number) {
            return get(number) == PRIVATE;
        }
        public static final boolean isPayphone(String number) {
            return get(number) == PAYPHONE;
        }
    }

    // コンポーネントの有効・無効切替え
    private static final int COMPONENT_ENABLED  = PackageManager.COMPONENT_ENABLED_STATE_ENABLED ;
    private static final int COMPONENT_DISABLED = PackageManager.COMPONENT_ENABLED_STATE_DISABLED;

    /**
     * コンポーネントの有効・無効を切替える..
     * @param context
     * @param clazz
     * @param enable
     */
    public static final void setComponentEnabled(Context context, Class<?> clazz, boolean enable) {
        PackageManager pm = context.getPackageManager();
        ComponentName component = new ComponentName(context, clazz);
        if (enable) {
            pm.setComponentEnabledSetting(component, COMPONENT_ENABLED , PackageManager.DONT_KILL_APP);
        } else {
            pm.setComponentEnabledSetting(component, COMPONENT_DISABLED, PackageManager.DONT_KILL_APP);
        }
    }

    /**
     * コンポーネントの有効・無効を取得.
     * @param context
     * @param clazz
     * @return
     */
    public static final boolean getComponetnEnabled(Context context, Class<?> clazz) {
        PackageManager pm = context.getPackageManager();
        ComponentName component = new ComponentName(context, clazz);
        return pm.getComponentEnabledSetting(component) == COMPONENT_ENABLED;
    }

    /**
     * ITelephonyのインスタンスを取得
     * @param context
     * @return ITelephony, 失敗したら'null'
     */
    public static final Object connectToTelephonyService(Context context) {
        if (!OUtil.hasPermission(context, Manifest.permission.MODIFY_PHONE_STATE)) {
            return null;
        }
        try {
            TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
            Class<?> clazz = Class.forName(tm.getClass().getName());
            Method m = clazz.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            return m.invoke(tm);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * ソフトキーボードの表示.
     * @param context
     * @param view
     */
    public static final void showSoftInput(final Context context, final View view) {
        view.requestFocus();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, 0);
            }
        }, 100); // MEMO: ちょっと時間を置かないと表示されない？
    }

    /**
     * ソフトキーボードの非表示.
     * @param context
     */
    public static final void hideSoftInput(Context context, View view) {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 入力時バイブレーションの設定を取得.
     * @param context
     * @return
     */
    public static final boolean hapticFeedbackEnabled(Context context) {
        return System.getInt(context.getContentResolver(), System.HAPTIC_FEEDBACK_ENABLED, 0) == 1;
    }

    /**
     * 指定されたパーミッションがあるかを確認する.
     * @param context
     * @param permission
     * @return
     */
    public static final boolean hasPermission(Context context, String permission) {
        int res = context.checkCallingOrSelfPermission(permission);
        return res == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 既定値を使用せず選択させる暗黙のインテント.
     * @param context
     * @param intent
     * @param titleId
     */
    public static final void sendChoiceIntent(Context context, Intent intent, int titleId) {
        Intent chooser = Intent.createChooser(intent, context.getText(titleId));
        context.startActivity(chooser);
    }

    /**
     * 文字列からバイト配列への変換.
     * @param data
     * @return
     */
    public static final byte[] stringToByteArray(String data) {
        int len = data.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; ++i) {
            int j = i * 2;
            int tmp = Integer.parseInt(data.substring(j, j + 2), 16);
            if (tmp > 127) {
                result[i] = (byte)(tmp - 256);
            } else {
                result[i] = (byte)tmp;
            }
        }
        return result;
    }

    /**
     * バイト配列から文字列への変換.
     * @param data
     * @return
     */
    public static final String byteArrayToString(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length * 2);
        for (byte b : data) {
            String tmp = Integer.toHexString(b < 0 ? b + 256 : b);
            if (tmp.length() == 1) {
                sb.append('0');
            }
            sb.append(tmp.toUpperCase());
        }
        return sb.toString();
    }

}
