package jp.gr.java_conf.ofnhwx.olib.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;

/**
 * ビットマップを便利に使うための自分用クラス.
 * @author yuta
 */
public final class OBitmap {

    private OBitmap() {}

    /**
     * Bitmapのサイズを取得するための{@link BitmapFactory.Options}を作成する.
     * @return `<code>{@link BitmapFactory.Options#inJustDecodeBounds} = true</code>'が設定された<code>Options</code>
     */
    private static final BitmapFactory.Options createOptions() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPurgeable = true;
        opt.inJustDecodeBounds = true;
        return opt;
    }

    /**
     * Bitmapをリサイズして作成するための{@link BitmapFactory.Options}を作成する.
     * @param base {@link #createOptions()}で作成した<code>Options</code>
     * @param max 縦(or 横)の最大サイズ
     * @return サイズ関連の設定を行った<code>Options</code>
     */
    private static final BitmapFactory.Options createOptions(BitmapFactory.Options base, float max) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPurgeable      = true;
        opt.inInputShareable = true;
        opt.inSampleSize     = (int)(Math.max(base.outHeight, base.outWidth) / max);
        return opt;
    }

    /**
     * DrawableからBitmap画像をリサイズして取得.
     * @param drawable
     * @return Bitmap画像(drawableが'null'なら'null'を返す)
     */
    public static final Bitmap getBitmap(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        Bitmap b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(b);
        drawable.setBounds(0, 0, w, h);
        drawable.draw(canvas);
        return b;
    }

    /**
     * DrawableからBitmap画像をリサイズして取得.
     * @param drawable
     * @param size リサイズ後のサイズ(幅・高さ)
     * @return リサイズしたBitmap画像(drawableが'null'なら'null'を返す)
     */
    public static final Bitmap getBitmap(Drawable drawable, int size) {
        Bitmap bitmap = getBitmap(drawable);
        Bitmap result = resizeBitmap(bitmap, size);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    /**
     * リソースIDからBitmap画像を生成.
     * @param context
     * @param id Bitmap画像のリソースID
     * @param max 縦・横の最大サイズ
     * @return Bitmap画像
     */
    private static final Bitmap decodeResourceInternal(Context context, int id, int max) {
        Resources res = context.getResources();
        BitmapFactory.Options options = createOptions();
        BitmapFactory.decodeResource(res, id, options);
        return BitmapFactory.decodeResource(res, id, createOptions(options, max));
    }

    /**
     * リソースIDからBitmap画像をリサイズして取得.
     * @param context
     * @param id Bitmap画像のリソースID
     * @param width リサイズ後の幅
     * @param height リサイズ後の高さ
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeResource(Context context, int id, int width, int height) {
        Bitmap bitmap = decodeResourceInternal(context, id, Math.max(width, height));
        Bitmap result = resizeBitmap(bitmap, width, height);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    /**
     * リソースIDからBitmap画像をリサイズして取得.
     * @param context
     * @param id Bitmap画像のリソースID
     * @param size リサイズ後のサイズ(幅・高さ)
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeResource(Context context, int id, int size) {
        Bitmap bitmap = decodeResourceInternal(context, id, size);
        Bitmap result =  resizeBitmap(bitmap, size);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    /**
     * リソースIDからBitmap画像をリサイズして取得.
     * @param context
     * @param id Bitmap画像のリソースID
     * @param size リサイズ後のサイズ(幅・高さ)
     * @param keepAspect アスペクト比を維持してリサイズするかどうか
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeResource(Context context, int id, int size, boolean keepAspect) {
        Bitmap bitmap = decodeResourceInternal(context, id, size);
        Bitmap result = resizeBitmap(bitmap, size, keepAspect);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    // ---------- Bitmap画像のリサイズ(バイトストリーム) ---------- //

    /**
     * バイト配列からBitmap画像を生成.
     * @param data Bitmap画像のバイト配列
     * @param max
     * @return Bitmap画像
     */
    private static final Bitmap decodeByteArrayInternal(byte[] data, float max) {
        if (data == null) {
            return null;
        }
        BitmapFactory.Options options = createOptions();
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        return BitmapFactory.decodeByteArray(data, 0, data.length, createOptions(options, max));
    }

    /**
     * バイト配列からBitmap画像をリサイズして取得.
     * @param data Bitmap画像のバイト配列
     * @param width リサイズ後の幅
     * @param height リサイズ後の高さ
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeByteArray(byte[] data, int width, int height) {
        Bitmap bitmap = decodeByteArrayInternal(data, Math.max(width, height));
        Bitmap result =  resizeBitmap(bitmap, width, height);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    /**
     * バイト配列からBitmap画像をリサイズして取得.
     * @param data Bitmap画像のバイト配列
     * @param size リサイズ後のサイズ(幅・高さ)
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeByteArray(byte[] data, int size) {
        Bitmap bitmap = decodeByteArrayInternal(data, size);
        Bitmap result = resizeBitmap(bitmap, size);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    /**
     * バイト配列からBitmap画像をリサイズして取得.
     * @param data Bitmap画像のバイト配列
     * @param size リサイズ後のサイズ(幅・高さ)
     * @param keepAspect アスペクト比を維持してリサイズするかどうか
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeByteArray(byte[] data, int size, boolean keepAspect) {
        Bitmap bitmap = decodeByteArrayInternal(data, size);
        Bitmap result = resizeBitmap(bitmap, size, keepAspect);
        if (bitmap != null) {
            bitmap = null;
        }
        return result;
    }

    /**
     * Uriの指定する内容をバイト配列に格納して返す.
     * @param context
     * @return
     */
    private static final byte[] getBytesInternal(Context context, Uri uri) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        OutputStream os = new BufferedOutputStream(result);
        InputStream  is = null;
        try {
            int c;
            is = context.getContentResolver().openInputStream(uri);
            while ((c = is.read()) != -1) {
                os.write(c);
            }
            os.flush();
        } catch (Exception e) {
            return null;
        } finally {
            try {
                is.close();
            } catch (Exception e) {
            }
        }
        return result.toByteArray();
    }

    /**
     * UriからBitmap画像をリサイズして取得.
     * @param context
     * @param uri
     * @param width リサイズ後の幅
     * @param height リサイズ後の高さ
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeUri(Context context, Uri uri, int width, int height) {
        byte[] bitmap = getBytesInternal(context, uri);
        return decodeByteArray(bitmap, width, height);
    }

    /**
     * UriからBitmap画像をリサイズして取得.
     * @param context
     * @param uri
     * @param size リサイズ後のサイズ(幅・高さ)
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeUri(Context context, Uri uri, int size) {
        byte[] bitmap = getBytesInternal(context, uri);
        return decodeByteArray(bitmap, size);
    }

    /**
     * UriからBitmap画像をリサイズして取得.
     * @param context
     * @param uri
     * @param size リサイズ後のサイズ(幅・高さ)
     * @param keepAspect アスペクト比を維持してリサイズするかどうか
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap decodeUri(Context context, Uri uri, int size, boolean keepAspect) {
        byte[] bitmap = getBytesInternal(context, uri);
        return decodeByteArray(bitmap, size, keepAspect);
    }

    /**
     * Bitmap画像のリサイズ.
     * @param bitmap リサイズするBitmap画像
     * @param width  リサイズ後の幅
     * @param height リサイズ後の高さ
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap resizeBitmap(Bitmap bitmap, int width, int height) {
        if (bitmap == null) {
            return null;
        }
        // リサイズ前のチェック
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        if (w == width && h == height) {
            return bitmap;
        }
        // リサイズ
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    /**
     * Bitmap画像のリサイズ.
     * @param bitmap リサイズするBitmap画像
     * @param size   リサイズ後のサイズ(幅・高さ)
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap resizeBitmap(Bitmap bitmap, int size) {
        return resizeBitmap(bitmap, size, size);
    }

    /**
     * Bitmap画像のリサイズ.
     * @param bitmap リサイズするBitmap画像
     * @param size   リサイズ後のサイズ(幅・高さ)
     * @param keepAspect アスペクト比を維持してリサイズするかどうか
     * @return リサイズしたBitmap画像(bitmapが'null'なら'null'を返す)
     */
    public static final Bitmap resizeBitmap(Bitmap bitmap, int size, boolean keepAspect) {
        if (bitmap == null) {
            return null;
        }
        // アスペクト比関連でいろいろと作業
        int width  = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width == height || !keepAspect) {
            return resizeBitmap(bitmap, size, size);
        } else if (width > height) {
            return resizeBitmap(bitmap, size, size * height / width);
        } else if (width < height) {
            return resizeBitmap(bitmap, size * width / height, size);
        }
        return null;
    }

}
