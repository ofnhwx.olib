package jp.gr.java_conf.ofnhwx.olib.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

/**
 * 自分用デバッグ機能集.
 * @author yuta
 */
public final class ODebug {

    private static final String DEBUG_PACKAGE = "jp.gr.java_conf.ofnhwx.debug";
    private static final String SU_PACKAGE    = "com.noshufou.android.su";

    private ODebug() {}

    private static final boolean findPackage(Context context, String packageName) {
        try {
            PackageManager pm = context.getPackageManager();
            pm.getApplicationInfo(packageName, 0);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    /**
     * デバッグ機能の有効・無効確認.
     * @param context
     * @return
     */
    public static final boolean isEnabled(Context context) {
        return findPackage(context, DEBUG_PACKAGE);
    }

    /**
     * root環境の判定.`Superuser'のインストール状態で判定を行う.
     * @param context
     * @return
     */
    public static final boolean isRooted(Context context) {
        return findPackage(context, SU_PACKAGE);
    }

}
