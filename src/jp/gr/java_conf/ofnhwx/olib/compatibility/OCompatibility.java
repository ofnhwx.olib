package jp.gr.java_conf.ofnhwx.olib.compatibility;

import java.lang.reflect.Method;

import android.app.Activity;
import android.os.Build;
import android.util.Log;

/**
 * 互換性を確保するための関数.
 * @author yuta
 */
public final class OCompatibility {

    private static final String TAG = OCompatibility.class.getSimpleName();

    private OCompatibility() {}

    public static final class Version {

        private Version() {}

        public static final boolean isAndroid21() {
            return Build.VERSION.SDK_INT > 4;
        }

        public static final boolean isAndroid30() {
            return Build.VERSION.SDK_INT > 10;
        }

        public static final boolean isAndroid40() {
            return Build.VERSION.SDK_INT > 13;
        }

    }

    public static final class MenuItem {

        private MenuItem() {}

        public static final int SHOW_AS_ACTION_NEVER     = 0;
        public static final int SHOW_AS_ACTION_IF_ROOM   = 1;
        public static final int SHOW_AS_ACTION_ALWAYS    = 2;
        public static final int SHOW_AS_ACTION_WITH_TEXT = 4;
        public static final int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;

    }

    public static final void invalidateOptionsMenu(Activity activity) {
        if (!Version.isAndroid30()) {
            return;
        }
        try {
            Method m = activity.getClass().getMethod("invalidateOptionsMenu");
            m.invoke(activity);
        } catch (Exception e) {
            Log.e(TAG, "invalidateOptionsMenu", e);
        }
    }

}
