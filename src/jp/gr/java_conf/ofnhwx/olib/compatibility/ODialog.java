package jp.gr.java_conf.ofnhwx.olib.compatibility;

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

/**
 * 自分用の簡易ダイアログユーティリティ.
 * @author yuta
 */
public final class ODialog {

    @SuppressLint("UseSparseArrays")
    private static final Map<Integer, Bundle> sBundleMap = new HashMap<Integer, Bundle>();

    private ODialog() {}

    /**
     * {@link #showDialog}で渡した{@link Bundle}を取得.
     * @param dialogId {@link #showDialog}で渡した<code>id</code>
     * @return {@link Bundle}
     */
    public static final Bundle getBundle(int dialogId) {
        Bundle bundle = sBundleMap.get(dialogId);
        if (bundle == null) {
            return new Bundle();
        } else {
            return bundle;
        }
    }

    /**
     * {@link #showDialog}で渡した{@link Bundle}を破棄.
     * @param dialogId {@link #showDialog}で渡した<code>id</code>
     * @return {@link Bundle}
     */
    public static final Bundle removeBundle(int dialogId) {
        Bundle bundle = sBundleMap.remove(dialogId);
        if (bundle == null) {
            return new Bundle();
        } else {
            return bundle;
        }
    }

    /**
     * {@link Activity#showDialog}でパラメータを渡すための小細工.
     * @param activity ダイアログを表示するアクティビティ
     * @param dialogId ダイアログのID
     * @param bundle ダイアログに渡すパラメータ
     */
    public static final void showDialog(Activity activity, int dialogId, Bundle bundle) {
        sBundleMap.put(dialogId, bundle);
        activity.showDialog(dialogId);
    }

    /**
     * {@link Activity#showDialog}でパラメータを渡すための小細工.
     * @param activity ダイアログを表示しているアクティビティ
     * @param dialogId ダイアログのID
     */
    public static final void dismissDialog(Activity activity, int dialogId) {
        removeBundle(dialogId);
        activity.dismissDialog(dialogId);
    }

    /**
     * {@link Activity#showDialog}でパラメータを渡すための小細工.
     * @param activity ダイアログを表示しているアクティビティ
     * @param dialogId ダイアログのID
     */
    public static final void removeDialog(Activity activity, int dialogId) {
        removeBundle(dialogId);
        activity.removeDialog(dialogId);
    }

}
