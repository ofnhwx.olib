package jp.gr.java_conf.ofnhwx.olib.compatibility;

import jp.gr.java_conf.ofnhwx.olib.compatibility.OCompatibility.Version;
import android.net.Uri;

/**
 * 電話帳の互換性対策.
 * @author yuta
 */
public final class OContacts {

    public static final Uri PHONES_CONTENT_FILTER_URL;
    public static final Uri PHONES_CONTENT_URI;
    public static final Uri PHOTOS_CONTENT_URI;
    public static final String ID;
    public static final String NAME;
    public static final String NUMBER;
    public static final String PHOTO_ID;
    public static final String PHOTO;
    public static final String STARRED;

    static {
        if (Version.isAndroid21()) {
            // ----- Android 1.6 より後 ----- //
            PHONES_CONTENT_FILTER_URL = Uri.parse("content://com.android.contacts/phone_lookup");
            PHONES_CONTENT_URI = Uri.parse("content://com.android.contacts/data/phones");
            PHOTOS_CONTENT_URI = Uri.parse("content://com.android.contacts/data");
            ID       = "_id";
            NAME     = "display_name";
            NUMBER   = "data1";
            PHOTO_ID = "photo_id";
            PHOTO    = "data15";
            STARRED  = "starred";
        } else {
            // ----- Android 1.6 まで ----- //
            PHONES_CONTENT_FILTER_URL = Uri.parse("content://contacts/phones/filter");
            PHONES_CONTENT_URI = Uri.parse("content://contacts/phones");
            PHOTOS_CONTENT_URI = Uri.parse("content://contacts/photos");
            ID       = "_id";
            NAME     = "display_name";
            NUMBER   = "number";
            PHOTO_ID = "person";
            PHOTO    = "data";
            STARRED  = "starred";
        }
    }

    private OContacts() {};

}
